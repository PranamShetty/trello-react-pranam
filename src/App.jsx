import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './App.css';


function App() {
  const [boardData, setBoardData] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const apiKey = '28ceeb8e6613d4d7dc24fc93f653a483';
      const id = '1';
      const apiToken = `ATTA0f2e314ccf953fbc182d9bb237cbe9703a69f6b38a7e3bd5bf6726fddf0bab588D7225DC`

 const url = `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${apiToken}`;


      try {
        const response = await axios.get(url);
        console.log(response.data);
      } catch (error) {
        console.log(error);
      }
    };

    fetchData();
  }, []);


  return (
    <div className="board">
      {boardData.map((board) => {
        console.log(board.name);
        return (
          <div className="board-card" key={board.id}>
            <h2>{board.name}</h2>
            <p>{board.desc}</p>
          </div>
        );
      })}
    </div>
  );
  




  
}



export default App;




